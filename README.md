# C++ Project Templates

A collection of basic C++ project templates.

## Building

> :information_source: This instruction describes how to run CMake commands manually. You can simply open
the `CMakeLists.txt` in any IDE that supports CMake, including but not limited to Qt Creator,
Visual Studio, KDevelop, and CLion.

1. Install `git`, a C++ compiler, `CMake`, and a build system available as a `CMake` generator
(e.g. `Ninja`, `GNU Make`, `MSBuild`, `NMake`, `Jom`).
2. Download this repository.
    ```bash
    $ git clone https://gitlab.com/pzlabs/cpp-project-templates.git
    ```
3. Copy the selected template to a separate directory.
    ```bash
    $ cp -R ${TEMPLATE_DIR} ${MY_PROJECT_DIR}
    $ cd ${MY_PROJECT_DIR}
    ```
3. Run the configuration step in the build directory. Change the name of this directory if needed.
Multiple build directories are supported and encouraged in case of multiple configurations.
`CMAKE_BUILD_TYPE` parameter can be any of `Debug`, `Release`, `RelWithDebInfo`, `MinSizeRel`.
    ```bash
    $ cmake -B build -D CMAKE_BUILD_TYPE=Debug
    ```
4. Build every target.
    ```bash
    $ cmake --build build
    ```

    To build a specific executable, run `cmake --build {MY_BUILD_DIR} --target ${MY_TARGET}`:
    ```bash
    $ cmake --build build --target hello_world
    ```

5. You can cleanup binaries in the build directory by running the `clean` target:
    ```bash
    $ cmake --build build --target clean
    ```

## Running

After being built, the binaries can be found in the build directory:
```bash
$ build/hello_world
```
