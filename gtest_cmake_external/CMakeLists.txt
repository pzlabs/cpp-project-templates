cmake_minimum_required(VERSION 3.21)

project(gtest_cmake_external)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
set(CMAKE_CXX_EXTENSIONS FALSE)

add_library(mylib STATIC
	src/mylib/mylib.hpp
	src/mylib/mylib.cpp
)
add_library(mylib::mylib ALIAS mylib)

target_include_directories(mylib PUBLIC src)

# Include CTest only if we are in the root directory
if(CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
	include(CTest)
endif()

if(BUILD_TESTING)
	find_package(GTest REQUIRED)
	
	add_executable(mylib_test
		test/test_mylib.cpp
	)

	target_link_libraries(mylib_test PRIVATE
		mylib::mylib
		GTest::gtest_main
	)
	target_include_directories(mylib_test PRIVATE test)

	include(GoogleTest)
	gtest_discover_tests(mylib_test)
endif()
