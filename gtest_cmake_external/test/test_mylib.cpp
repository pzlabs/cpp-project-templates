#include <cmath>
#include <limits>

#include <gtest/gtest.h>

#include "mylib/mylib.hpp"

// NOTE: Anonymous namespace prevents potential linker errors: all functions and globals defined
// inside `namespace { .. }` have internal linkage. In other words, we are effectively putting
// `static` specifier on each function and global variable (though it's not strictly necessary)
namespace {

TEST(cube, ValidInput) {
	// NOTE: Here `EXPECT_EQ` is replaced with `EXPECT_DOUBLE_EQ` which checks that two `double`s
	// are approximately equal. Floating point types can't represent every real number, so testing
	// for exact equality doesn't always work
	EXPECT_DOUBLE_EQ(mylib::cube(4.), 64.);
	EXPECT_DOUBLE_EQ(mylib::cube(6.5), 274.625);
	EXPECT_DOUBLE_EQ(mylib::cube(-2.7), -19.683);
	EXPECT_DOUBLE_EQ(mylib::cube(-2.7e5), -19.683e15);
}

// We need wrappers for <cmath> functions because creating a pointer to a standard function
// is illegal (https://eel.is/c++draft/namespace.std#6)
bool is_nan(double value) { return std::isnan(value); }
bool is_positive_inf(double value) { return std::isinf(value) && !std::signbit(value); }
bool is_negative_inf(double value) { return std::isinf(value) && std::signbit(value); }
bool is_positive_zero(double value) { return value == 0. && !std::signbit(value); }
bool is_negative_zero(double value) { return value == 0. && std::signbit(value); }

// NOTE: The following tests only make sense in an environment that follows the floating point
// rules defined in the IEC 559 (IEEE 754) standard.
// See https://en.wikipedia.org/wiki/Double-precision_floating-point_format
static_assert(std::numeric_limits<double>::is_iec559, "Unsupported platform");

TEST(cube, EdgeCases) {
	// NOTE: We could call `is_positive_zero(mylib::cube(+0))` directly, but `EXPECT_PRED*`
	// macros enable printing of argument values
	EXPECT_PRED1(is_positive_zero, mylib::cube(+0.));
	EXPECT_PRED1(is_negative_zero, mylib::cube(-0.));

	// NOTE: Using `EXPECT_EQ` is safe here because pow(1, 3) can be calculated without the loss of
	// precision
	EXPECT_EQ(mylib::cube(+1.), +1.);
	EXPECT_EQ(mylib::cube(-1.), -1.);

	// NaN*NaN is still a NaN as specified by IEEE 754
	EXPECT_PRED1(is_nan, mylib::cube(std::nan("")));

	EXPECT_PRED1(is_positive_inf, mylib::cube(std::numeric_limits<double>::max()));
	EXPECT_PRED1(is_negative_inf, mylib::cube(std::numeric_limits<double>::lowest()));

	EXPECT_PRED1(is_positive_inf, mylib::cube(std::numeric_limits<double>::infinity()));
	EXPECT_PRED1(is_negative_inf, mylib::cube(-std::numeric_limits<double>::infinity()));
}

TEST(cube, Overflow) {
	// `double` doesn't have enough exponent bits to represent extremely large numbers, such as
	// pow(10, 450)
	EXPECT_PRED1(is_positive_inf, mylib::cube(+1e150));
	EXPECT_PRED1(is_negative_inf, mylib::cube(-1e150));
}

TEST(cube, Underflow) {
	// `double` doesn't have enough exponent bits to represent extremely small numbers, such as
	// pow(10, -450). Nonetheless, the sign bit should be saved after underflowing multiplications
	EXPECT_PRED1(is_positive_zero, mylib::cube(+1e-150));
	EXPECT_PRED1(is_negative_zero, mylib::cube(-1e-150));

	EXPECT_PRED1(is_positive_zero, mylib::cube(std::numeric_limits<double>::min()));
	EXPECT_PRED1(is_negative_zero, mylib::cube(-std::numeric_limits<double>::min()));
}

} // namespace

