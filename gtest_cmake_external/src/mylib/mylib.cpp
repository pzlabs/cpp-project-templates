#include "mylib/mylib.hpp"

namespace mylib {

double cube(double x) {
	return x * x * x;
}

} // namespace mylib
