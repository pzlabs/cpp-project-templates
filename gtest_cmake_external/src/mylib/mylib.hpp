#ifndef GTEST_CMAKE_EXTERNAL_MYLIB_HPP_INCLUDED
#define GTEST_CMAKE_EXTERNAL_MYLIB_HPP_INCLUDED

namespace mylib {

double cube(double x);

} // namespace mylib
#endif // include guard
