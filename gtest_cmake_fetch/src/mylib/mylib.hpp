#ifndef GTEST_CMAKE_FETCH_MYLIB_HPP_INCLUDED
#define GTEST_CMAKE_FETCH_MYLIB_HPP_INCLUDED

namespace mylib {

double cube(double x);

} // namespace mylib
#endif // include guard
