# CMake + GTest Project Template

A C++ project template showcasing integration of GoogleTest library into a CMake-based project.  
The GoogleTest framework is automatically downloaded from GitHub and built by CMake.

## Building

> :information_source: These instructions describe how to run CMake commands manually. You can
> simply open the `CMakeLists.txt` in any IDE that supports CMake, including Qt Creator,
> Visual Studio, KDevelop, and CLion.

1. Install `git`, a C++ compiler, `CMake`, and a build system available as a `CMake` generator
(e.g. `Ninja`, `GNU Make`, `MSBuild`, `NMake`, `Jom`).
2. Clone the Git repository.
    ```bash
    $ git clone https://gitlab.com/pzlabs/cpp-project-templates.git
    ```
3. Copy this template to a separate directory.
    ```bash
    $ cp -R cpp-project-templates/gtest_cmake_fetch ${MY_PROJECT_DIR}
    $ cd ${MY_PROJECT_DIR}
    ```
3. Run the configuration step in the build directory. Change the name of this directory if needed.
    ```bash
    $ cmake -B build -D CMAKE_BUILD_TYPE=Debug
    ```
4. Build every target.
    ```bash
    $ cmake --build build
    ```
5. If needed, you can cleanup binaries in the build directory by running the `clean` target:
    ```bash
    $ cmake --build build --target clean
    ```

## Running

Run all the tests using CTest wrapper:
```bash
$ ctest --test-dir build --output-on-failure
```

Alternatively, one can execute the binary directly from the build directory:
```bash
$ build/mylib_test
```
